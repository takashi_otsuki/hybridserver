# AIWolf Hybrid Server

In addition to TCP/IP connection from remote/non-Java client,
this server can accept direct connection from local Java client
in order to reduce the overhead of transmitting packets to loopback.
The current version is 1.0.0.

## Timeout Option
The timeout option limits the time for agent's processing one request.
If a request call runs out of time, the server writes a message like this:

`INITIALIZE@TOT aborts because of timeout(100ms).`

and terminates throwing TimeoutException.
You can tune your agent taking into account of the message.

# AIWolf Hybrid Game Starter

AIwolf game starter which can run local Java agents and remote/non-Java agents together.
This is an example of the utilization of hybrid server.

## Usage
For example on windows, at the folder in which AIWolf platform is extracted,
execute the following command to start game described in the configuration file.

` java -cp aiwolf-common.jar;aiwolf-server.jar;aiwolf-client.jar;aiwolf-viewer.jar;jsonic-1.3.10.jar;HybridServer-1.0.0.jar jp.gr.java_conf.otk.aiwolf.HybridGameStarter -f configFile [-g gameNum] [-l logFolder] [-p port] [-t timeout]`

HybridGameStarter reads the configuration from `configFile`.
The number of games, log folder, port and timeout set in the configuration file
can be overwritten by command line arguments `-g`, `-l`, `-p` and `-t` respectively.

## Configuration file
Here is the example of the configuration file.

    # Whether or not the game runs in aiwolf-ver0.2 compatible mode.
    # In case of this mode, all the agents except the network connected
    # ones are created at the beginning of every game, on the contrary,
    # in normal mode, the agents' instances are created only once at
    # the beginning of the starter and reused through all the games.
    # yes or no(default)
    compat_old_version=no
    
    # Whether or not the network connected agent is forced to reconnect
    # at the beginning of every game.
    # This is only valid in aiwolf-ver0.2 compatible mode.
    # yes or no(default)
    force_reconnect=no
    
    # Whether or not the game log is saved in file.
    # yes or no(default)
    savelog=no
    
    # Whether or not the progress log is written to the standard output.
    # yes or no(default)
    consolelog=no
    
    # Whether or not the uttered text is validated.
    # yes(default) or no
    validate_utterance=yes
    
    # Library path.
    # Default value is current folder.
    lib=./lib/
    
    # The name of the top folder in which the game logs are saved.
    # Default value is "./log/".
    log=./log/
    
    # Port number.
    # Default value is 10000.
    port=10000
    
    # The number of games.
    # Default value is 1.
    game=1
    
    # Maximum time(ms) which agent is allowed to consume every request.
    # Default value is 100.
    timeout=100
    
    # The number of agents in the game.
    # If the number of agents described in this file is less than this
    # value, the starter waits until the vacant agents are filled with
    # the agents connected via TCP/IP.
    # Default value is 15.
    agent=5
    
    # List of agents in CSV format.
    # name(optional),jar file(required),class name(required),requested role(optional),comment(ignored)
    player1,aiwolf-client.jar,org.aiwolf.sample.player.SampleRoleAssignPlayer,WEREWOLF,Sample
    player2,aiwolf-client.jar,org.aiwolf.sample.player.SampleRoleAssignPlayer,SEER,Sample
    player3,aiwolf-client.jar,org.aiwolf.sample.player.SampleRoleAssignPlayer,,Sample
    player4,aiwolf-client.jar,org.aiwolf.sample.player.SampleRoleAssignPlayer,,Sample
    player5,aiwolf-client.jar,org.aiwolf.sample.player.SampleRoleAssignPlayer,,Sample

# TOClientStarter
TOClientStarter is the clone of the official client starter,
which can limit the time for agent's processing one request.
See "Timeout Option" above for further information.

## Usage
`TOClientStarter [-h host] [-p port] -c clientClass [requestRole] [-n name] [-t timeout]`

At default, values of `host`, `port` and `timeout` are `localhost`, `10000` and `100` respectively.

## Example
` java -cp aiwolf-common.jar;aiwolf-client.jar;jsonic-1.3.10.jar;HybridServer-1.0.0.jar jp.gr.java_conf.otk.aiwolf.TOClientStarter -h localhost -p 12345 -c org.aiwolf.client.base.smpl.SampleRoleAssignPlayer -t 100`