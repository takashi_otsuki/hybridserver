//
// TOTcpipClient.java
//
// Copyright (c) 2016 Takashi OTSUKI
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
//

package jp.gr.java_conf.otk.aiwolf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.aiwolf.common.AIWolfRuntimeException;
import org.aiwolf.common.data.Player;
import org.aiwolf.common.data.Request;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.data.Talk;
import org.aiwolf.common.net.DataConverter;
import org.aiwolf.common.net.GameClient;
import org.aiwolf.common.net.GameInfo;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.common.net.Packet;
import org.aiwolf.common.net.TalkToSend;

public class TOTcpipClient implements GameClient {
	private String host;
	private int port;
	private Socket socket;
	private boolean isRunning;
	private Player player;
	private Role requestRole;
	private GameInfo lastGameInfo;
	private String playerName;
	private long timeout = 100; // ms

	public TOTcpipClient(String host, int port, Player player) {
		this.host = host;
		this.port = port;
		this.player = player;
		isRunning = false;
	}

	public TOTcpipClient(String host, int port, Player player, Role requestRole) {
		this(host, port, player);
		this.requestRole = requestRole;
	}

	public TOTcpipClient(String host, int port, Player player, Role requestRole, String playerName) {
		this(host, port, player, requestRole);
		this.playerName = playerName;
	}

	public void connect() {
		socket = new Socket();
		try {
			socket.connect(new InetSocketAddress(host, port));
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		try {
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				Packet packet = DataConverter.getInstance().toPacket(line);
				Object obj = recieve(packet);
				if (packet.getRequest().hasReturn()) {
					if (obj == null) {
						bw.append("\n");
					} else if (obj instanceof String) {
						bw.append(obj + "\n");
					} else {
						bw.append(DataConverter.getInstance().convert(obj) + "\n");
					}
					bw.flush();
				}
			}
		} catch (IOException e) {
			if (isRunning) { // Finished abnormally
				throw new AIWolfRuntimeException(e);
			}
		}
	}

	/*
	 * (非 Javadoc)
	 * 
	 * @see org.aiwolf.server.sc.LocalConnectServer#recieve(org.aiwolf.server.sc.Packet)
	 */
	@Override
	public Object recieve(Packet packet) {

		GameInfo gameInfo = lastGameInfo;
		GameSetting gameSetting = packet.getGameSetting();

		if (packet.getGameInfo() != null) {
			gameInfo = packet.getGameInfo().toGameInfo();
			lastGameInfo = gameInfo;
		}

		if (packet.getTalkHistory() != null) {
			Talk lastTalk = null;
			if (gameInfo.getTalkList() != null && !gameInfo.getTalkList().isEmpty()) {
				lastTalk = gameInfo.getTalkList().get(gameInfo.getTalkList().size() - 1);
			}
			for (TalkToSend talk : packet.getTalkHistory()) {
				if (isAfter(talk, lastTalk)) {
					gameInfo.getTalkList().add(talk.toTalk());
				}
			}
		}

		if (packet.getWhisperHistory() != null) {
			Talk lastWhisper = null;
			if (gameInfo.getWhisperList() != null && !gameInfo.getWhisperList().isEmpty()) {
				lastWhisper = gameInfo.getWhisperList().get(gameInfo.getWhisperList().size() - 1);
			}
			for (TalkToSend whisper : packet.getWhisperHistory()) {
				if (isAfter(whisper, lastWhisper)) {
					gameInfo.getWhisperList().add(whisper.toTalk());
				}
			}
		}

		RequestTask t = new RequestTask(gameInfo, gameSetting, packet.getRequest());
		ExecutorService pool = Executors.newSingleThreadExecutor();
		Future<Object> future = pool.submit(t);
		Object returnObject = null;
		try {
			returnObject = future.get(timeout, TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
			System.err.println(packet.getRequest() + "@" + player.getName() + " aborts because of timeout(" + timeout + "ms).");
			e.printStackTrace();
			System.exit(0);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			pool.shutdown();
		}
		return returnObject;
	}

	/**
	 * Check is talk after lastTalk.<br>
	 * If it is same, return false;
	 * 
	 * @param talk
	 * @param lastTalk
	 * @return
	 */
	private boolean isAfter(TalkToSend talk, Talk lastTalk) {
		if (lastTalk != null) {
			if (talk.getDay() < lastTalk.getDay()) {
				return false;
			}
			if (talk.getDay() == lastTalk.getDay() && talk.getIdx() <= lastTalk.getIdx()) {
				return false;
			}
		}
		return true;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	private class RequestTask implements Callable<Object> {
		private GameInfo gameInfo;
		private GameSetting gameSetting;
		private Request request;

		public RequestTask(GameInfo gameInfo, GameSetting gameSetting, Request request) {
			this.gameInfo = gameInfo;
			this.gameSetting = gameSetting;
			this.request = request;
		}

		@Override
		public Object call() throws InterruptedException {
			Object returnObject = null;
			// Call the player's method according to the request.
			switch (request) {
			case INITIALIZE:
				player.initialize(gameInfo, gameSetting);
				break;
			case DAILY_INITIALIZE:
				player.update(gameInfo);
				player.dayStart();
				break;
			case DAILY_FINISH:
				player.update(gameInfo);
				break;
			case NAME:
				if (playerName == null) {
					playerName = player.getName();
					if (playerName == null) {
						playerName = player.getClass().getName();
					}
				}
				returnObject = playerName;
				break;
			case ROLE:
				if (requestRole != null) {
					returnObject = requestRole.toString();
				} else {
					returnObject = "none";
				}
				break;
			case ATTACK:
				player.update(gameInfo);
				returnObject = player.attack();
				break;
			case TALK:
				player.update(gameInfo);
				returnObject = player.talk();
				if (returnObject == null) {
					returnObject = Talk.SKIP;
				}
				break;
			case WHISPER:
				player.update(gameInfo);
				returnObject = player.whisper();
				if (returnObject == null) {
					returnObject = Talk.SKIP;
				}
				break;
			case DIVINE:
				player.update(gameInfo);
				returnObject = player.divine();
				break;
			case GUARD:
				player.update(gameInfo);
				returnObject = player.guard();
				break;
			case VOTE:
				player.update(gameInfo);
				returnObject = player.vote();
				break;
			case FINISH:
				player.update(gameInfo);
				player.finish();
				isRunning = false;
			default:
				break;
			}
			return returnObject;
		}
	}
}
