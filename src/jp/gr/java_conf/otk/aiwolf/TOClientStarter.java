//
// TOClientStarter.java
//
// Copyright (c) 2016 Takashi OTSUKI
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
//

package jp.gr.java_conf.otk.aiwolf;

import java.io.IOException;

import org.aiwolf.common.data.Player;
import org.aiwolf.common.data.Role;

public class TOClientStarter {

	public static void main(String[] args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		String host = "localhost";
		int port = 10000;

		String clsName = null;
		Role roleRequest = null;
		String playerName = null;
		long timeout = 100;

		for (int i = 0; i < args.length; i++) {
			if (args[i].startsWith("-")) {
				if (args[i].equals("-p")) {
					i++;
					port = Integer.parseInt(args[i]);
				} else if (args[i].equals("-h")) {
					i++;
					host = args[i];
				} else if (args[i].equals("-c")) {
					i++;
					clsName = args[i];
					i++;
					try {
						if (i > args.length - 1 || args[i].startsWith("-")) {
							i--;
							roleRequest = null;
							continue;
						}
						roleRequest = Role.valueOf(args[i]);
					} catch (IllegalArgumentException e) {
						System.err.println("No such role as " + args[i]);
						return;
					}

				} else if (args[i].equals("-n")) {
					i++;
					playerName = args[i];
				} else if (args[i].equals("-t")) {
					i++;
					timeout = Long.parseLong(args[i]);
				}
			}
		}
		if (port < 0 || clsName == null) {
			System.err.println("Usage:" + TOClientStarter.class + " [-h host] [-p port] -c clientClass [requestRole] [-n name] [-t timeout]");
			return;
		}

		Player player = (Player) Class.forName(clsName).newInstance();
		TOTcpipClient client = new TOTcpipClient(host, port, player, roleRequest, playerName);
		client.setTimeout(timeout);
		client.connect();
	}
}
