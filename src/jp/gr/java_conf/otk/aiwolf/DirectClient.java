//
// DirectClient.java
//
// Copyright (c) 2016 Takashi OTSUKI
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
//

package jp.gr.java_conf.otk.aiwolf;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.aiwolf.common.data.Player;
import org.aiwolf.common.data.Request;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.data.Talk;
import org.aiwolf.common.net.GameClient;
import org.aiwolf.common.net.GameInfo;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.common.net.Packet;
import org.aiwolf.common.net.TalkToSend;

/**
 * AIWolf game client direct communicating with the game server.
 * 
 * @author otsuki
 *
 */
public class DirectClient implements GameClient {
	private Player player;
	private Role requestRole;
	private GameInfo latestGameInfo;
	private String playerName;
	private long timeout = 100; // ms

	private class RequestTask implements Callable<Object> {
		private GameInfo gameInfo;
		private GameSetting gameSetting;
		private Request request;

		public RequestTask(GameInfo gameInfo, GameSetting gameSetting, Request request) {
			this.gameInfo = gameInfo;
			this.gameSetting = gameSetting;
			this.request = request;
		}

		@Override
		public Object call() throws InterruptedException {
			Object returnObject = null;
			// Call the player's method according to the request.
			switch (request) {
			case INITIALIZE:
				player.initialize(gameInfo, gameSetting);
				break;
			case DAILY_INITIALIZE:
				player.update(gameInfo);
				player.dayStart();
				break;
			case DAILY_FINISH:
				player.update(gameInfo);
				break;
			case NAME:
				if (playerName == null) {
					playerName = player.getName();
					if (playerName == null) {
						playerName = player.getClass().getName();
					}
				}
				returnObject = playerName;
				break;
			case ROLE:
				if (requestRole != null) {
					returnObject = requestRole.toString();
				} else {
					returnObject = "none";
				}
				break;
			case ATTACK:
				player.update(gameInfo);
				returnObject = player.attack();
				break;
			case TALK:
				player.update(gameInfo);
				returnObject = player.talk();
				if (returnObject == null) {
					returnObject = Talk.SKIP;
				}
				break;
			case WHISPER:
				player.update(gameInfo);
				returnObject = player.whisper();
				if (returnObject == null) {
					returnObject = Talk.SKIP;
				}
				break;
			case DIVINE:
				player.update(gameInfo);
				returnObject = player.divine();
				break;
			case GUARD:
				player.update(gameInfo);
				returnObject = player.guard();
				break;
			case VOTE:
				player.update(gameInfo);
				returnObject = player.vote();
				break;
			case FINISH:
				player.update(gameInfo);
				player.finish();
			default:
				break;
			}
			return returnObject;
		}
	}

	/**
	 * Initializes a new instance.
	 * 
	 * @param player
	 *            An instance of the player this client uses.
	 * @param requestRole
	 *            The player's role requested. Give null if no role is requested.
	 */
	public DirectClient(Player player, Role requestRole) {
		this.player = player;
		this.requestRole = requestRole;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.common.net.GameClient#recieve(org.aiwolf.common.net.Packet)
	 */
	@Override
	public Object recieve(Packet packet) {

		GameInfo gameInfo = latestGameInfo;
		GameSetting gameSetting = packet.getGameSetting();

		// Retrieve the latest game information from the received packet.
		if (packet.getGameInfo() != null) {
			gameInfo = packet.getGameInfo().toGameInfo();
			latestGameInfo = gameInfo;
		}

		// Append new talks to the game information.
		if (packet.getTalkHistory() != null) {
			Talk lastTalk = null;
			if (gameInfo.getTalkList() != null && !gameInfo.getTalkList().isEmpty()) {
				lastTalk = gameInfo.getTalkList().get(gameInfo.getTalkList().size() - 1);
			}
			for (TalkToSend talk : packet.getTalkHistory()) {
				if (isAfter(talk, lastTalk)) {
					gameInfo.getTalkList().add(talk.toTalk());
				}
			}
		}

		// Append new whispers to the game information.
		if (packet.getWhisperHistory() != null) {
			Talk lastWhisper = null;
			if (gameInfo.getWhisperList() != null && !gameInfo.getWhisperList().isEmpty()) {
				lastWhisper = gameInfo.getWhisperList().get(gameInfo.getWhisperList().size() - 1);
			}
			for (TalkToSend whisper : packet.getWhisperHistory()) {
				if (isAfter(whisper, lastWhisper)) {
					gameInfo.getWhisperList().add(whisper.toTalk());
				}
			}
		}

		RequestTask t = new RequestTask(gameInfo, gameSetting, packet.getRequest());
		ExecutorService pool = Executors.newSingleThreadExecutor();
		Future<Object> future = pool.submit(t);
		Object returnObject = null;
		try {
			returnObject = future.get(timeout, TimeUnit.MILLISECONDS);
		} catch (TimeoutException e) {
			System.err.println(packet.getRequest() + "@" + playerName + " aborts because of timeout(" + timeout + "ms).");
			e.printStackTrace();
			System.exit(0);
		} catch (InterruptedException | ExecutionException e) {
			e.printStackTrace();
			System.exit(0);
		} finally {
			pool.shutdown();
		}
		return returnObject;
	}

	/**
	 * Returns true if the given talk is after the latest talk known.
	 * 
	 * @param talk
	 *            Talk to be checked.
	 * @param latestTalk
	 *            The latest talk known.
	 * @return
	 */
	private boolean isAfter(TalkToSend talk, Talk latestTalk) {
		if (latestTalk != null) {
			if (talk.getDay() < latestTalk.getDay()) {
				return false;
			}
			if (talk.getDay() == latestTalk.getDay() && talk.getIdx() <= latestTalk.getIdx()) {
				return false;
			}
		}
		return true;
	}

	public Role getRequestRole() {
		return requestRole;
	}

	public void setRequestRole(Role requestRole) {
		this.requestRole = requestRole;
	}

	public String getName() {
		return playerName;
	}

	public void setName(String name) {
		this.playerName = name;
	}

	public void setTimeout(long timeout) {
		this.timeout = timeout;
	}
}
