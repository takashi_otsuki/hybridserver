//
// HybridGameStarter.java
//
// Copyright (c) 2016 Takashi OTSUKI
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
//

package jp.gr.java_conf.otk.aiwolf;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.aiwolf.common.data.Player;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.common.util.CalendarTools;
import org.aiwolf.server.AIWolfGame;
import org.aiwolf.server.util.FileGameLogger;
import org.aiwolf.ui.util.AgentLibraryReader;

/**
 * AIWolf game starter using both direct connection and TCP/IP connection.
 * 
 * @author otsuki
 *
 */
public class HybridGameStarter {

	private class PlayerInfo {
		public String name;
		public String jarFileName;
		public String className;
		public Role requestRole;
		@SuppressWarnings("rawtypes")
		public Class playerClass;

		public PlayerInfo(String name, String jarFileName, String className, Role requestRole) {
			this.name = name;
			this.jarFileName = jarFileName;
			this.className = className;
			this.requestRole = requestRole;
		}
	}

	private List<PlayerInfo> playerList = new ArrayList<>();

	@SuppressWarnings("rawtypes")
	private Map<String, Class> classMap = new HashMap<>();;

	private int agentNum = 15;
	private int gameNum = 1;
	private int port = 10000;
	private String logDirName = "./log/";
	private boolean isShowConsoleLog = false;
	private boolean isSaveLog = false;
	private boolean forceReconnect = false;
	private boolean isValidateUtterance = true;
	private boolean isCompatOldVersion = false;
	private long timeout = 100;

	/**
	 * Initializes a new instance.
	 * 
	 * @param fileName
	 *            The name of configuration file.
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("rawtypes")
	public HybridGameStarter(String fileName) throws IOException, ClassNotFoundException {
		String libraryDir = ".";
		Path src = new File(fileName).toPath();
		for (String line : Files.readAllLines(src, Charset.forName("UTF8"))) {
			if (line.startsWith("#")) {
				continue;
			} else if (line.contains("=")) {
				String[] data = line.split("=");
				if (data.length < 2) {
					continue;
				}
				if (data[0].trim().equalsIgnoreCase("lib")) {
					libraryDir = data[1].trim();
				} else if (data[0].trim().equalsIgnoreCase("log")) {
					logDirName = data[1].trim();
				} else if (data[0].trim().equalsIgnoreCase("port")) {
					port = Integer.parseInt(data[1].trim());
				} else if (data[0].trim().equalsIgnoreCase("agent")) {
					agentNum = Integer.parseInt(data[1].trim());
				} else if (data[0].trim().equalsIgnoreCase("game")) {
					gameNum = Integer.parseInt(data[1].trim());
				} else if (data[0].trim().equalsIgnoreCase("timeout")) {
					timeout = Long.parseLong(data[1].trim());
				} else if (data[0].trim().equalsIgnoreCase("compat_old_version")) {
					if (data[1].trim().matches("^[YyTt].*")) {
						isCompatOldVersion = true;
					}
				} else if (data[0].trim().equalsIgnoreCase("consolelog")) {
					if (data[1].trim().matches("^[YyTt].*")) {
						isShowConsoleLog = true;
					}
				} else if (data[0].trim().equalsIgnoreCase("savelog")) {
					if (data[1].trim().matches("^[YyTt].*")) {
						isSaveLog = true;
					}
				} else if (data[0].trim().equalsIgnoreCase("force_reconnect")) {
					if (data[1].trim().matches("^[YyTt].*")) {
						forceReconnect = true;
					}
				} else if (data[0].trim().equalsIgnoreCase("validate_utterance")) {
					if (data[1].trim().matches("^[NnFf].*")) {
						isValidateUtterance = false;
					}
				}
			} else {
				String[] data = line.split(",");
				if (data.length < 3) {
					continue;
				}
				String name = data[0];
				String jarFileName = data[1].isEmpty() ? null : Paths.get(libraryDir, data[1]).toString();
				String className = data[2];
				Role role = null;
				if (data.length >= 4) {
					try {
						role = Role.valueOf(data[3]);
					} catch (IllegalArgumentException e) {
					}
				}
				playerList.add(new PlayerInfo(name, jarFileName, className, role));
			}
		}

		for (PlayerInfo info : playerList) {
			if (info.jarFileName == null) {
				info.playerClass = Class.forName(info.className);
			} else {
				for (Class c : AgentLibraryReader.getPlayerClassList(new File(info.jarFileName))) {
					if (info.className.equals(c.getName())) {
						info.playerClass = c;
						break;
					}
				}
			}
			if (info.playerClass == null) {
				System.err.println("Can not load " + info.className + " from " + info.jarFileName);
				System.exit(0);
			}
		}
	}

	/**
	 * Sets the number of games.
	 * 
	 * @param gameNum
	 */
	public void setGameNum(int gameNum) {
		if (gameNum > 0) {
			this.gameNum = gameNum;
		}
	}

	/**
	 * Sets timeout value.
	 * 
	 * @param timeout
	 */
	public void setTimeout(int timeout) {
		if (timeout > 0) {
			this.timeout = timeout;
		}
	}

	/**
	 * Sets port number.
	 * 
	 * @param port
	 */
	public void setPort(int port) {
		if (port > 1000) {
			this.port = port;
		}
	}

	/**
	 * Sets the name of log directory.
	 * 
	 * @param logDirName
	 */
	public void setLogDir(String logDirName) {
		if (logDirName != null) {
			this.logDirName = logDirName;
		}
	}

	/**
	 * Starts the games.
	 * 
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InterruptedException
	 * @throws IOException
	 */
	public void gameStart() throws InstantiationException, IllegalAccessException, InterruptedException, IOException {
		String subLogDirName = logDirName + "/" + CalendarTools.toDateTime(System.currentTimeMillis()).replaceAll("[\\s-/:]", "");
		GameSetting gameSetting = GameSetting.getDefaultGame(agentNum);
		gameSetting.setValidateUtterance(isValidateUtterance);
		HybridServer gameServer = new HybridServer(port, agentNum, gameSetting);

		System.err.printf("Start AIWolf Hybrid Server (port:%d playerNum:%d ", port, agentNum);

		if (!isCompatOldVersion) {
			System.err.printf(")\n");
			ArrayList<DirectClient> clients = new ArrayList<DirectClient>();
			for (int j = 0; j < playerList.size(); j++) {
				if (j < agentNum) {
					PlayerInfo pi = playerList.get(j);
					DirectClient client = new DirectClient((Player) pi.playerClass.newInstance(), pi.requestRole);
					client.setTimeout(timeout);
					if (pi.name.length() > 0) {
						client.setName(pi.name);
					}
					clients.add(client);
				}
			}
			gameServer.connectClients(clients, false);
			for (int i = 0; i < gameNum; i++) {
				AIWolfGame game = new AIWolfGame(gameSetting, gameServer);
				game.setShowConsoleLog(isShowConsoleLog);
				if (isSaveLog) {
					File logFile = new File(String.format("%s/%03d.log", subLogDirName, i));
					game.setGameLogger(new FileGameLogger(logFile));
				}
				game.start();
			}
			gameServer.close();
		} else {
			System.err.printf("aiwolf-ver0.2 compatible mode)\n");
			ArrayList<DirectClient> clients = new ArrayList<DirectClient>();
			for (int i = 0; i < gameNum; i++) {
				clients.clear();
				for (int j = 0; j < playerList.size(); j++) {
					if (j < agentNum) {
						PlayerInfo pi = playerList.get(j);
						DirectClient client = new DirectClient((Player) pi.playerClass.newInstance(), pi.requestRole);
						client.setTimeout(timeout);
						if (pi.name.length() > 0) {
							client.setName(pi.name);
						}
						clients.add(client);
					}
				}
				gameServer.connectClients(clients, forceReconnect);
				AIWolfGame game = new AIWolfGame(gameSetting, gameServer);
				game.setShowConsoleLog(isShowConsoleLog);
				if (isSaveLog) {
					File logFile = new File(String.format("%s/%03d.log", subLogDirName, i));
					game.setGameLogger(new FileGameLogger(logFile));
				}
				game.start();
			}
		}
	}

	private static void usage() {
		System.err.println("Usage: HybridGameStarter -f initFileName [-g gameNum] [-l logDir] [-p port] [-t timeout]");
		System.exit(0);
	}

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InterruptedException, IOException {
		String initFileName = null;
		String logDir = null;
		int gameNum = 0;
		int timeout = 0;
		int port = 0;

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-f")) {
				i++;
				initFileName = args[i];
			} else if (args[i].equals("-g")) {
				i++;
				gameNum = Integer.parseInt(args[i]);
			} else if (args[i].equals("-l")) {
				i++;
				logDir = args[i];
			} else if (args[i].equals("-p")) {
				i++;
				port = Integer.parseInt(args[i]);
			} else if (args[i].equals("-t")) {
				i++;
				timeout = Integer.parseInt(args[i]);
			} else {
				usage();
			}
		}
		if (initFileName == null) {
			usage();
		}

		HybridGameStarter starter = new HybridGameStarter(initFileName);

		starter.setGameNum(gameNum);
		starter.setLogDir(logDir);
		starter.setPort(port);
		starter.setTimeout(timeout);
		starter.gameStart();
	}

}
