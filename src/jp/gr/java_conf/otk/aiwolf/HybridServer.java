//
// HybridServer.java
//
// Copyright (c) 2016 Takashi OTSUKI
//
// This software is released under the MIT License.
// http://opensource.org/licenses/mit-license.php
//

package jp.gr.java_conf.otk.aiwolf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.aiwolf.client.lib.Content;
import org.aiwolf.common.data.Agent;
import org.aiwolf.common.data.Request;
import org.aiwolf.common.data.Role;
import org.aiwolf.common.net.DataConverter;
import org.aiwolf.common.net.GameSetting;
import org.aiwolf.common.net.Packet;
import org.aiwolf.common.net.TalkToSend;
import org.aiwolf.common.util.BidiMap;
import org.aiwolf.server.GameData;
import org.aiwolf.server.LostClientException;
import org.aiwolf.server.net.GameServer;

/**
 * AIWolf game server which accepts both direct connection and TCP/IP connection.
 * 
 * @author otsuki
 *
 */
public class HybridServer implements GameServer {

	private class Connection {
		public boolean isSocket;
		public Socket socket;
		public DirectClient client;
		public BufferedWriter writer;
		public BufferedReader reader;

		public Connection(Socket socket) {
			isSocket = true;
			this.socket = socket;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		public Connection(DirectClient client) {
			isSocket = false;
			this.client = client;
		}

		public void close() {
			if (isSocket) {
				try {
					reader.close();
					writer.close();
					socket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private int port;
	private int limit;
	private BidiMap<Connection, Agent> connectionAgentMap;
	private GameData gameData;
	private GameSetting gameSetting;
	private Logger serverLogger;
	private Map<Agent, String> agentNameMap;
	private Map<Agent, Integer> lastTalkIdxMap;
	private Map<Agent, Integer> lastWhisperIdxMap;
	private ServerSocket serverSocket;

	/**
	 * Initializes a new instance.
	 * 
	 * @param port
	 *            Port number.
	 * @param limit
	 *            The upper limit of the number of clients connected.
	 * @param gameSetting
	 *            The settings of the game.
	 */
	public HybridServer(int port, int limit, GameSetting gameSetting) {
		this.gameSetting = gameSetting;
		this.port = port;
		this.limit = limit;

		String loggerName = this.getClass().getSimpleName();
		serverLogger = Logger.getLogger(loggerName);
		agentNameMap = new HashMap<>();
		lastTalkIdxMap = new HashMap<Agent, Integer>();
		lastWhisperIdxMap = new HashMap<Agent, Integer>();
	}

	/**
	 * Connects this server to the clients direct and via TCP/IP.
	 * 
	 * @param clients
	 *            The list of clients to be connected direct.
	 * @param reconect
	 *            Whether or not the network clients are reconnected.
	 * @throws IOException
	 */
	public void connectClients(List<DirectClient> clients, boolean reconnect) throws IOException {
		// For randomizing the mapping between clients and agent indexes.
		ArrayList<Integer> randomList = new ArrayList<>();
		for (int i = 1; i <= limit; i++) {
			randomList.add(new Integer(i));
		}
		Collections.shuffle(randomList);

		Map<Connection, Agent> lastConnectionAgentMap = connectionAgentMap;
		connectionAgentMap = new BidiMap<>();
		agentNameMap.clear();

		// Register direct clients.
		for (int i = 0; i < clients.size(); i++) {
			if (connectionAgentMap.size() < limit) {
				Agent agent = Agent.getAgent(randomList.get(i));
				connectionAgentMap.put(new Connection(clients.get(i)), agent);
				requestName(agent); // This updates agentNameMap.
			}
		}

		// The first game or reconnection.
		if (lastConnectionAgentMap == null || reconnect) {
			if (lastConnectionAgentMap != null) {
				// Close sockets used in the last game.
				for (Connection con : lastConnectionAgentMap.keySet()) {
					if (con.isSocket && con.socket != null && con.socket.isConnected()) {
						con.socket.close();
					}
				}
			}

			// Register network clients.
			if (connectionAgentMap.size() < limit) {
				System.err.println("Waiting for TCP/IP connection...\n");
				serverSocket = new ServerSocket(port);
				for (int i = connectionAgentMap.size(); i < limit; i++) {
					Socket socket = serverSocket.accept();
					Agent agent = Agent.getAgent(randomList.get(i));
					connectionAgentMap.put(new Connection(socket), agent);
					requestName(agent); // This updates agentNameMap.
				}
				serverSocket.close();
			}
		} else {
			// Reuse sockets used in the last game.
			int i = connectionAgentMap.size();
			for (Connection con : lastConnectionAgentMap.keySet()) {
				if (con.isSocket && con.socket != null && con.socket.isConnected()) {
					Agent agent = Agent.getAgent(randomList.get(i++));
					connectionAgentMap.put(con, agent);
					requestName(agent); // This updates agentNameMap.
				}
				if (i >= limit) {
					break;
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#getConnectedAgentList()
	 */
	@Override
	public List<Agent> getConnectedAgentList() {
		return new ArrayList<Agent>(connectionAgentMap.values());
	}

	private Object send(Agent agent, Request request) {
		Packet packet;
		if (request == Request.DAILY_INITIALIZE || request == Request.INITIALIZE) {
			lastTalkIdxMap.clear();
			lastWhisperIdxMap.clear();
			packet = new Packet(request, gameData.getGameInfoToSend(agent), gameSetting);
		} else if (request == Request.NAME || request == Request.ROLE) {
			packet = new Packet(request);
		} else if (request != Request.FINISH) {
			if ((request == Request.VOTE && !gameData.getLatestVoteList().isEmpty()) || (request == Request.ATTACK && !gameData.getLatestAttackVoteList().isEmpty())
					|| (gameData.getExecuted() != null && (request == Request.DIVINE || request == Request.GUARD || request == Request.WHISPER || request == Request.ATTACK))) {
				packet = new Packet(request, gameData.getGameInfoToSend(agent));
			} else {
				List<TalkToSend> talkList = gameData.getGameInfoToSend(agent).getTalkList();
				List<TalkToSend> whisperList = gameData.getGameInfoToSend(agent).getWhisperList();
				talkList = minimize(agent, talkList, lastTalkIdxMap);
				whisperList = minimize(agent, whisperList, lastWhisperIdxMap);
				packet = new Packet(request, talkList, whisperList);
			}
		} else {
			packet = new Packet(request, gameData.getFinalGameInfoToSend(agent));
		}

		Connection connection = connectionAgentMap.getKey(agent);
		if (connection.isSocket) {
			String message = DataConverter.getInstance().convert(packet);
			serverLogger.info("=>" + agent + ":" + message);
			try {
				connection.writer.append(message);
				connection.writer.append("\n");
				connection.writer.flush();
			} catch (IOException e) {
				throw new LostClientException("Lost connection with " + agent + "\t" + getName(agent), e, agent);
			}
		} else {
			serverLogger.info("=>" + agent + ":" + DataConverter.getInstance().convert(packet));
			return connection.client.recieve(packet);
		}
		return null;
	}

	/**
	 * Delete the talks already sent to the agent.
	 * 
	 * @param agent
	 * @param list
	 * @param lastIdxMap
	 * @return
	 */
	private List<TalkToSend> minimize(Agent agent, List<TalkToSend> list, Map<Agent, Integer> lastIdxMap) {
		int lastIdx = list.size();
		if (lastIdxMap.containsKey(agent) && list.size() >= lastIdxMap.get(agent)) {
			list = list.subList(lastIdxMap.get(agent), list.size());
		}
		lastIdxMap.put(agent, lastIdx);
		return list;
	}

	private Object request(Agent agent, Request request) {
		Connection connection = connectionAgentMap.getKey(agent);
		if (connection.isSocket) {
			try {
				String line;
				send(agent, request);
				line = connection.reader.readLine().trim();
				serverLogger.info("<=" + agent + ":" + line);
				if (line != null && line.isEmpty()) {
					line = null;
				}
				if (request == Request.NAME || request == Request.ROLE) {
					return line;
				} else if (request == Request.TALK || request == Request.WHISPER) {
					if (gameSetting.isValidateUtterance()) {
						if (Content.validate(line)) {
							return line;
						} else {
							return null;
						}
					} else {
						return line;
					}
				} else if (request == Request.ATTACK || request == Request.DIVINE || request == Request.GUARD || request == Request.VOTE) {
					return DataConverter.getInstance().toAgent(line);
				} else {
					return null;
				}
			} catch (IOException e) {
				throw new LostClientException("Lost connection with " + agent + "\t" + getName(agent), e, agent);
			}
		} else {
			Object obj = send(agent, request);
			serverLogger.info("<=" + agent + ":" + obj);
			return obj;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#init(org.aiwolf.common.data.Agent)
	 */
	@Override
	public void init(Agent agent) {
		send(agent, Request.INITIALIZE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#dayStart(org.aiwolf.common.data.Agent)
	 */
	@Override
	public void dayStart(Agent agent) {
		send(agent, Request.DAILY_INITIALIZE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#dayFinish(org.aiwolf.common.data.Agent)
	 */
	@Override
	public void dayFinish(Agent agent) {
		send(agent, Request.DAILY_FINISH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestName(org.aiwolf.common.data.Agent)
	 */
	@Override
	public String requestName(Agent agent) {
		if (agentNameMap.containsKey(agent)) {
			return agentNameMap.get(agent);
		} else {
			String name = (String) request(agent, Request.NAME);
			agentNameMap.put(agent, name);
			return name;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestRequestRole(org.aiwolf.common.data.Agent)
	 */
	@Override
	public Role requestRequestRole(Agent agent) {
		String roleString = (String) request(agent, Request.ROLE);
		try {
			return Role.valueOf(roleString);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestTalk(org.aiwolf.common.data.Agent)
	 */
	@Override
	public String requestTalk(Agent agent) {
		return (String) request(agent, Request.TALK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestWhisper(org.aiwolf.common.data.Agent)
	 */
	@Override
	public String requestWhisper(Agent agent) {
		return (String) request(agent, Request.WHISPER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestVote(org.aiwolf.common.data.Agent)
	 */
	@Override
	public Agent requestVote(Agent agent) {
		return (Agent) request(agent, Request.VOTE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestDivineTarget(org.aiwolf.common.data.Agent)
	 */
	@Override
	public Agent requestDivineTarget(Agent agent) {
		return (Agent) request(agent, Request.DIVINE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestGuardTarget(org.aiwolf.common.data.Agent)
	 */
	@Override
	public Agent requestGuardTarget(Agent agent) {
		return (Agent) request(agent, Request.GUARD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#requestAttackTarget(org.aiwolf.common.data.Agent)
	 */
	@Override
	public Agent requestAttackTarget(Agent agent) {
		return (Agent) request(agent, Request.ATTACK);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#finish(org.aiwolf.common.data.Agent)
	 */
	@Override
	public void finish(Agent agent) {
		send(agent, Request.FINISH);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#setGameData(org.aiwolf.server.GameData)
	 */
	@Override
	public void setGameData(GameData gameData) {
		this.gameData = gameData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#setGameSetting(org.aiwolf.common.net.GameSetting)
	 */
	@Override
	public void setGameSetting(GameSetting gameSetting) {
		this.gameSetting = gameSetting;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.aiwolf.server.net.GameServer#close()
	 */
	@Override
	public void close() {
		try {
			if (serverSocket != null && !serverSocket.isClosed()) {
				serverSocket.close();
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for (Connection connection : connectionAgentMap.keySet()) {
			if (connection.isSocket) {
				connection.close();
			}
		}
		connectionAgentMap.clear();
		agentNameMap.clear();
	}

	public Logger getServerLogger() {
		return serverLogger;
	}

	public void setServerLogger(Logger serverLogger) {
		this.serverLogger = serverLogger;
	}

	public String getName(Agent agent) {
		return agentNameMap.get(agent);
	}
}
